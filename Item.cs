﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tabler
{
    // Класс, элемент таблицы.
    // Используется при хранении выделенных данных.
    class Item : IComparable
    {
        public string Column { get; set; }
        public string Value { get; set; }

        // Удаляет из коллекции элемент по тексту и названию столбца
        internal static void Remove(List<Item> selectedItems, string text, string column)
        {
            foreach (var element in selectedItems)
            {
                if (element.Value == text && element.Column == column)
                {
                    selectedItems.Remove(element);
                    break;
                }
            }
        }

        public int CompareTo(object obj)
        {
            return Value.CompareTo(((Item)obj).Value);
        }
    }
}
