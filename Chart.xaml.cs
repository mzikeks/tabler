﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tabler
{
    /// <summary>
    /// Interaction logic for Histogram.xaml
    /// Универсальное окно для отображения гистограмм и графиков.
    /// </summary>
    public partial class Chart : Window
    {

        public int barsCount  { get; private set; } = 5;
        internal List<Item> selectedItems;
        public string selectedColumn;

        public Chart()
        {
            InitializeComponent();
        }

        // События изменения числа столбцов в гистограмме, нужно перерисовать ее.
        private void BarCountSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            barsCount = (int)e.NewValue;
            if (selectedItems != null)
            {
                DrawHistogram();
            }
        }

        // Метод, разделяющий текстовые данные на группы и вычисляющий, как часто встречаются данные группы.
        internal int[] GetTextFrequences(List<Item> selectedItems, ref string[] barsNames)
        {
            selectedItems.Sort();
            int[] frequences = new int[barsCount];
            Dictionary<string, int> counts = new Dictionary<string, int>();

            foreach (var value in selectedItems)
            {
                if (counts.ContainsKey(value.Value))
                {
                    counts[value.Value]++;
                }
                else
                {
                    counts[value.Value] = 1;
                }
            }
            string[] keys = counts.Keys.ToArray();
            int perBarKeys = Math.Max(1, counts.Count / (barsCount - 1));

            for (int barNumber = 0; barNumber < barsCount - 1; barNumber++)
            {
                int n = 0;
                for (int i = barNumber * perBarKeys; i < Math.Min(counts.Count, barNumber * perBarKeys + perBarKeys); i++)
                {
                    barsNames[barNumber] += keys[i] + ", ";
                    n += counts[keys[i]];
                }
                frequences[barNumber] = n;
            }
            return frequences;
        }

        // Метод для разделения численных значений на интервалы и нахождения количества элементов в каждом интервале.
        internal int[] GetNumericFrequences(List<double> numericValues, ref string[] columnsNames)
        {
            numericValues.Sort();
            double min = numericValues[0];
            double step = (numericValues[numericValues.Count - 1] - min) / (barsCount - 1);

            for (int i = 0; i < columnsNames.Length; i++)
            {
                columnsNames[i] = $"[{(min + step * i).ToString("f2")} - {(min + step * (i + 1)).ToString("f2")}]";
            }

            int[] frequences = new int[barsCount];

            foreach (var value in numericValues)
            {
                //MessageBox.Show($"{value} {min} {step} {(int)((value - min) / step)} {(value - min) / step}");
                frequences[(int)((value - min) / step)]++;
            }
            return frequences;
        }

        // Отрисовывает гистограмму LiveChart.
        internal void DrawHistogram()
        {
            bool isAllNumeric = true;
            var numericValues = new List<double>();
            foreach (var value in selectedItems)
            {
                if (double.TryParse(value.Value, out double valueDouble))
                {
                    numericValues.Add(valueDouble);
                    continue;
                }
                isAllNumeric = false;
                break;
            }

            // Два массива одинаковых размеров - высота столбца и его подпись.
            int[] frequences;
            string[] columnsNames = new string[barsCount];

            if (isAllNumeric && numericValues.Count > 0)
            {
                frequences = GetNumericFrequences(numericValues, ref columnsNames);
            }
            else
            {
                frequences = GetTextFrequences(selectedItems, ref columnsNames);
            }
            SeriesCollection histogram = new SeriesCollection {
                    new ColumnSeries{ Values = new ChartValues<int>(frequences),
                                      DataLabels = true
                                      }};
            
            AxisX.Labels = columnsNames;
            HistogramChart.Series = histogram;
            AxisY.Title = "count";
        }

        // Отрисовывает график зависимости x от y.
        internal void DrawGraph()
        {
            string column1Name = selectedItems[0].Column;
            string column2Name = selectedItems[0].Column;

            var numericValuesCol1 = new List<double>();
            var numericValuesCol2 = new List<double>();
            foreach (var value in selectedItems)
            {
                if (double.TryParse(value.Value, out double valueDouble))
                {
                    if (value.Column == column1Name) numericValuesCol1.Add(valueDouble);
                    else
                    {
                        column2Name = value.Column;
                        numericValuesCol2.Add(valueDouble);
                    }
                    continue;
                }
                MessageBox.Show("Ошибка! Некоторые из выбранных значений не являются числовыми");
                return;
            }

            ChartValues<ObservablePoint> points = new ChartValues<ObservablePoint>();

            for (int i = 0; i < numericValuesCol1.Count; i++)
            {
                points.Add(new ObservablePoint
                {
                    X = numericValuesCol1[i],
                    Y = numericValuesCol2[i]
                });
            }
            var sortedPointsList = points.OrderBy(u => u.X).ToList();
            ChartValues<ObservablePoint> sortedPoints = new ChartValues<ObservablePoint>(sortedPointsList);


            SeriesCollection chart = new SeriesCollection { new LineSeries { Values = sortedPoints} };
            HistogramControllers.Visibility = Visibility.Hidden;
            HistogramChart.Series = chart;
            AxisX.Title = column1Name;
            AxisY.Title = column2Name;
        }
    }
}
