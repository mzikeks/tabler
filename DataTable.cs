﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;

namespace Tabler
{
    internal class DataTable
    {
        public static readonly char separator = ',';
        // Название столбцов.
        public List<string> Head { get; }
        // Матрица с данными.
        public ObservableCollection<List<string>> Table { get; }

        public DataTable(List<string> head, ObservableCollection<List<string>> table)
        {
            this.Table = table;
            this.Head = head; 
        }

        // Метод, возвращаяет DataTable по имени.
        internal static DataTable GetTable(string filename)
        {
            try
            {
                using (var streamReader = new StreamReader(filename))
                {
                    var header = SplitRow(streamReader.ReadLine());
                    var tableList = new ObservableCollection<List<string>>();
                    string line;

                    while ((line = streamReader.ReadLine()) != null)
                    {
                        tableList.Add(SplitRow(line));
                    }
                    return new DataTable(header, tableList);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException("Ошибка! Не удается открыть указанный файл");
            }
        }

        // Метод, разбивающий строку по запятым, и оставляющий запятые внути кавычев.
        private static List<string> SplitRow(string v)
        {
            var splitedList = new List<string>();
            var splitedArray = v.Split(new char[] { '"' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < splitedArray.Length; i++)
            {
                if (splitedArray[i] == ",")
                {
                    continue;
                }
                else if (i % 2 != 0)
                {
                    splitedList.Add(splitedArray[i].Trim(','));
                }
                else
                {
                    foreach (var substring in splitedArray[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        splitedList.Add(substring);
                    }
                }
            }

            return splitedList;
        }
    }
}