﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using System.Threading;
using System.Globalization;
using LiveCharts.Defaults;

// Используется nuget package Syncfusion.Charts.Wpf.

namespace Tabler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataTable activeTable;

        public ObservableCollection<List<string>> ActiveTable
        {
            get
            {
                return activeTable.Table;
            }
        }
        List<Item> selectedItems = new List<Item>();
        public string selectedColumn;

        public MainWindow()
        {
            InitializeComponent(); 
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
        }

        // Загрузка таблицы по имени.
        private void LoadTable(string filename)
        {
            activeTable = DataTable.GetTable(filename);
            MainTable.ItemsSource = ActiveTable;

            MainTable.Columns.Clear();
            for (int i = 0; i < ActiveTable[0].Count; ++i)
            {
                MainTable.Columns.Add(new DataGridTextColumn { Binding = new Binding("[" + i.ToString() + "]"),                                                 Header = activeTable.Head[i] }) ;
            }
        }
        
        // Обработчик события нажатия кнопки открытия таблицы
        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "csv files (*.csv)|*.csv";
            openFileDialog.Title = "Выберите таблицу csv для открытия";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    LoadTable(openFileDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        // Обработчик события, когда изменились выделенные клетки таблицы
        private void MainTable_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            foreach (var cellInfo in e.AddedCells)
            {
                selectedColumn = cellInfo.Column.Header.ToString();
                var tb = (TextBlock)(cellInfo.Column.GetCellContent(cellInfo.Item));
                if (tb != null)
                {
                    selectedItems.Add(new Item { Column = selectedColumn, Value = tb.Text });
                }
                else
                {
                    selectedItems = new List<Item>();
                    break;
                }
            }
            foreach (var cellInfo in e.RemovedCells)
            {
                var tb = (TextBlock)(cellInfo.Column.GetCellContent(cellInfo.Item));
                selectedColumn = cellInfo.Column.Header.ToString();

                if (tb != null)
                {selectedColumn = cellInfo.Column.Header.ToString();
                    Item.Remove(selectedItems, tb.Text, selectedColumn);
                }
                else
                {
                    selectedItems = new List<Item>();
                    break;
                }
            }
            UpdateStatistic();
        }
        
        // Метод, обновляющий статистику выделенного фрагмента
        private void UpdateStatistic()
        {
            InfoMeanTextBlock.Text = "-";
            InfoMedianTextBlock.Text = "-";
            InfoDeviationTextBlock.Text = "-";
            InfoVarianceTextBlock.Text = "-";
            List<double> values = new List<double>();

            foreach (var value in selectedItems)
            {
                var content = value.Value.ToString();
                if (content != null && double.TryParse(content, out double valueDouble))
                {
                    values.Add(valueDouble);
                    continue;
                }
                return;
            }

            if (values.Count == 0) return;
            values.Sort();

            InfoMeanTextBlock.Text = (values.Sum() / values.Count()).ToString("f02");
            InfoMedianTextBlock.Text = values[values.Count / 2].ToString("f02");
            InfoDeviationTextBlock.Text = GetVariance(values).ToString("f02");
            InfoVarianceTextBlock.Text = Math.Sqrt(GetVariance(values)).ToString("f02");

        }

        // Метод, вычисляющий СКО.
        private double GetVariance(List<double> values)
        {
            double sum = 0;
            double mean = values.Sum() / values.Count();
            foreach (var element in values)
            {
                sum += (element - mean) * (element - mean);
            }
            return sum / values.Count();
        }

        // Обработчкий события нажатия на кнопку построения диаграммы.
        private void ShowHistogrammButton_Click(object sender, RoutedEventArgs e)
        {
            Chart histogramWindow = new Chart { selectedItems = selectedItems, selectedColumn = selectedColumn };
            histogramWindow.DrawHistogram();
            histogramWindow.Show();
        }

        // Обработчик события нажатия на кнопку для построения графика зависимости x от y.
        private void ShowGraphButton_Click(object sender, RoutedEventArgs e)
        {
            HashSet<string> uniqueColums = new HashSet<string>();
            foreach (var element in selectedItems)
            {
                uniqueColums.Add(element.Column);
            }
            if (uniqueColums.Count != 2)
            {
                MessageBox.Show("Ошибка! Для постоения графика должны быть выбраны ровно 2 столбца");
                return;
            }

            foreach (var value in selectedItems)
            {
                if (!double.TryParse(value.Value, out double valueDouble))
                {
                    MessageBox.Show("Ошибка! Некоторые из выбранных значений не являются числовыми");
                    return;
                }
            }

            Chart histogramWindow = new Chart{ selectedColumn = selectedColumn, selectedItems = selectedItems };
            histogramWindow.DrawGraph();
            histogramWindow.Show();
        }
    }
}
